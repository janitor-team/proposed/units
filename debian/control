Source: units
Maintainer: Stephen Kitt <skitt@debian.org>
Section: utils
Priority: optional
Build-Depends: bison,
               debhelper-compat (= 13),
               libreadline-dev,
               python3,
               texinfo
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/debian/units
Vcs-Git: https://salsa.debian.org/debian/units.git
Homepage: https://www.gnu.org/software/units/
Rules-Requires-Root: no

Package: units
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Recommends: python3, python3-requests
Description: converts between different systems of units
 GNU 'units' program converts quantities expressed in various scales
 to their equivalents in other scales.  The 'units' program can only
 handle multiplicative scale changes directly.  It uses a functional
 notation for non-proportional conversions such a Fahrenheit
 temperature to Celsius temperature.
 .
 This package includes the units_cur script to update currency
 conversion information; this script requires a Python 3 interpreter
 and the python3-requests library.
