units (2.22-2) unstable; urgency=medium

  * Fix the currency.units symlink. Closes: #1019337.

 -- Stephen Kitt <skitt@debian.org>  Wed, 07 Sep 2022 17:04:01 +0200

units (2.22-1) unstable; urgency=medium

  * New upstream release, merging all patches.
  * Disable the currency update check.
  * Update lintian overrides.
  * Standards-Version 4.6.1, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Tue, 06 Sep 2022 08:40:03 +0200

units (2.21-2) unstable; urgency=medium

  * Apply the patch provided by upstream to fix segfaults when performing
    verbose inverse lookups of table-driven units. Closes: #1003708.
  * Standards-Version 4.6.0, no change required.

 -- Stephen Kitt <skitt@debian.org>  Sun, 30 Jan 2022 21:16:37 +0100

units (2.21-1) unstable; urgency=medium

  * New upstream release.
  * Fix remaining troublesome dates in the changelog; thanks to Axel
    Beckert for pointing them out! Closes: #971974.
  * Bump debian/watch to version 4 and simplify it.
  * Update copyright information.
  * Standards-Version 4.5.1, no change required.

 -- Stephen Kitt <skitt@debian.org>  Sat, 21 Nov 2020 15:46:34 +0100

units (2.20-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit (from ./configure).
  * Fix day-of-week for changelog entry 1.55-2.
  * Update standards version to 4.4.1, no changes needed.

  [ Stephen Kitt ]
  * New upstream release.
  * Switch to debhelper compatibility level 13.
  * Standards-Version 4.5.0, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Sat, 10 Oct 2020 10:08:39 +0200

units (2.19-1) unstable; urgency=medium

  * Upload to unstable.
  * Standards-Version 4.4.0, no change required.

 -- Stephen Kitt <skitt@debian.org>  Thu, 15 Aug 2019 21:15:21 +0200

units (2.19-1~exp1) experimental; urgency=medium

  * New upstream release.
    - Continuation paragraphs are indented in the manpage.
      Closes: #921160.
    - The AUTHORS section of the manpage is now filled in.
      Closes: #926559.
  * Update debian/copyright.
  * Standards-Version 4.3.0, no further change required.
  * Switch to debhelper compatibility level 12.

 -- Stephen Kitt <skitt@debian.org>  Sat, 01 Jun 2019 14:09:02 +0200

units (2.18-1) unstable; urgency=medium

  * New upstream release.
    - New currency sources replace the Yahoo! service which is no longer
      available. Closes: #910503.
    - Downloaded data is validated before being interpreted.
      Closes: #902935.
  * Set “Rules-Requires-Root: no”.
  * Standards-Version 4.2.1, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Mon, 22 Oct 2018 10:22:02 +0200

units (2.17-3) unstable; urgency=medium

  * Actually remove the cron.daily file on upgrade.

 -- Stephen Kitt <skitt@debian.org>  Sun, 08 Jul 2018 23:36:59 +0200

units (2.17-2) unstable; urgency=medium

  * Avoid calculating the MD5 sum of the shipped currency.units since it
    is intended to be overwritten eventually. Closes: #902929.
  * Drop the cron job, it should be up to individual users when currencies
    are updated. Closes: #902890, #903317.

 -- Stephen Kitt <skitt@debian.org>  Sun, 08 Jul 2018 23:24:27 +0200

units (2.17-1) unstable; urgency=medium

  * New upstream release, handling Yahoo currency updates correctly.
    Closes: #900587.
  * Add a cron job to update currencies daily.

 -- Stephen Kitt <skitt@debian.org>  Tue, 26 Jun 2018 23:03:38 +0200

units (2.16-2) unstable; urgency=medium

  * Migrate to Salsa.
  * Explicitly recommend python3.
  * Switch to debhelper compatibility level 11.
  * Standards-Version 4.1.4, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Thu, 17 May 2018 00:46:42 +0200

units (2.16-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Thu, 02 Nov 2017 22:55:45 +0100

units (2.15-1) unstable; urgency=medium

  * New upstream release, using python3-requests instead of
    python3-unidecode.
  * Switch to a secured URL in debian/watch.
  * Standards-Version 4.1.1, no further change required.
  * Clean up debian/changelog whitespace.

 -- Stephen Kitt <skitt@debian.org>  Sun, 29 Oct 2017 20:29:18 +0100

units (2.14-1) unstable; urgency=medium

  * New upstream release.
  * Switch to debhelper compatibility level 10.

 -- Stephen Kitt <skitt@debian.org>  Mon, 19 Jun 2017 22:52:17 +0200

units (2.13-1) unstable; urgency=medium

  * New upstream version:
    - drop micro-second-fix.patch, merged upstream;
    - refresh no-currency-update.patch;
    - refresh manpage-typos.patch, partially merged upstream.
  * Clean the built object files so the package can be built twice in a
    row.
  * Enable all hardening options.
  * Clean up debian/control using cme.
  * Standards-Version 3.9.8, no further change required.
  * Switch to the Python 3 version of units_cur.
  * Add a manpage for units_cur.

 -- Stephen Kitt <skitt@debian.org>  Sun, 26 Jun 2016 15:50:10 +0200

units (2.12-2) unstable; urgency=medium

  * Switch to https: VCS URIs (see #810378).
  * Link to the actual homepage for units (thanks 積丹尼 Dan Jacobson;
    closes: #813275).
  * Correct the documentation for '-H' (thanks to Jakub Wilk; closes:
    #803679).
  * Describe °C—°F conversions as non-proportional rather than non-
    linear (thanks to Lionel Elie Mamane for the suggestion; closes:
    #410935).
  * Apply upstream fix for µs (closes: #813104).

 -- Stephen Kitt <skitt@debian.org>  Sun, 31 Jan 2016 16:15:28 +0100

units (2.12-1) unstable; urgency=medium

  * New upstream version.
  * Refresh patches, deleting texinfo.patch (merged upstream),
    preserve-cppflags.patch (merged upstream), and units_cur-warning.patch
    (fixed differently upstream).
  * Add signature verification information.
  * Use the postinst/postrm generated by dh_installmenu.
  * dh-python no longer supports ${python:Depends} in the "Recommends"
    stanza, so hard-code the Python dependencies instead.
  * Prevent currency updates during the build (this allows building
    twice in a row and avoids Internet access during the build).

 -- Stephen Kitt <skitt@debian.org>  Sun, 18 Oct 2015 23:27:09 +0200

units (2.11-2) unstable; urgency=medium

  * Document the requirement on unidecode (closes: #767864).
  * Avoid unidecode warnings when decoding ASCII strings; thanks to
    Hagen Fuchs for the patch!
  * Standards-Version 3.9.6, no change required.
  * Stop texi2man from including the date in its output, so that the build
    can be reproducible.

 -- Stephen Kitt <skitt@debian.org>  Thu, 30 Apr 2015 17:57:23 +0200

units (2.11-1) unstable; urgency=medium

  * New upstream version.

 -- Stephen Kitt <skitt@debian.org>  Wed, 09 Apr 2014 07:35:23 +0200

units (2.10-1) unstable; urgency=medium

  * New upstream version.
  * Refresh patches, deleting generic-python.patch (no longer relevant)
    and bisonfix.patch (merged upstream).
  * Add missing '/' to debian/watch to allow updating to the latest
    version.

 -- Stephen Kitt <skitt@debian.org>  Sun, 30 Mar 2014 00:06:25 +0100

units (2.02-2) unstable; urgency=medium

  * Add bisonfix.patch from upstream to allow building with bison 3
    (closes: #733374).
  * Standards-Version 3.9.5, no change required.

 -- Stephen Kitt <skitt@debian.org>  Sat, 04 Jan 2014 11:01:41 +0100

units (2.02-1) unstable; urgency=low

  * New upstream version.
  * Add texinfo.patch to fix @tex sections with texinfo 5.
  * Add preserve-cppflags.patch to keep CPPFLAGS.
  * Use debian/clean to delete files instead of overriding dh_clean;
    delete parse.tab.c and units.info since they're re-generated at build
    time, and build-depend on bison.
  * Install units_cur now that it works, and build-depend on python.

 -- Stephen Kitt <skitt@debian.org>  Thu, 18 Jul 2013 00:34:37 +0200

units (2.01-1) unstable; urgency=low

  * Adopt package (closes: #705770).
  * New upstream version (closes: #702543, LP: #1019786):
    - includes updated CODATA values (closes: #591526);
    - defines imperial pint as 20 imperial fluid ounces (closes: #637092);
    - updates manpage fixing all reported issues (closes: #606403,
      LP: #1009549);
    - adds Taiwanese units (closes: #604615);
    - explains how to view available units (closes: #661903, #704506);
    - adds the twip unit (closes: #520428).
  * Use simplified dh 9 rules (LP: #496810).
  * Specify source format "3.0 (quilt)" and remove units.man on clean (it
    is modified during the build).
  * Add watch file provided by Bart Martens (thanks!).
  * Drop obsolete build-dependency on texi2html.
  * Use dh_autoreconf to update the configure script automatically.
  * wrap-and-sort the control files.
  * Drop obsolete libreadline{5,6}-dev alternative build-dependencies.
  * Add collab-maint VCS information.
  * Standards-Version 3.9.4, no further change required.
  * Drop obsolete build-dependency on dpkg or install-info.
  * Add pager.patch to use /usr/bin/pager instead of more when PAGER is
    not defined (closes: #695909).
  * Update debian/copyright and switch to machine-readable format.

 -- Stephen Kitt <skitt@debian.org>  Sun, 07 Jul 2013 00:38:26 +0200

units (1.88-2) unstable; urgency=low

  * Orphaned

 -- John G. Hasler <jhasler@debian.org>  Fri, 19 Apr 2013 14:06:55 -0500

units (1.88-1) unstable; urgency=low

  * New upstream release

    Closes: #629926: Temperature units are not correctly converted
    Closes: #642973: currency exchange rates should be updated

 -- John G. Hasler <jhasler@debian.org>  Tue, 19 Jun 2012 20:16:52 -0500

units (1.87-2) unstable; urgency=low

  * Deleted debian/prerm, fixed debian/postinst and postrm.
    Added debian/compat (level 7), Edited debian/control
    Brought standards-version up to 3.8.3, updated libreadline
    build dependency, added dpkg (>= 1.15.4) | install-info to depends:

  * Replaced "more" with "/usr/bin/pager" at line 55 in units.c
    Closes: #548597: Please use /usr/bin/pager as default pager

 -- John G. Hasler <jhasler@debian.org>  Wed, 07 Oct 2009 21:34:00 -0500

units (1.87-1) unstable; urgency=low

  * New upstream release

    Upstream made "*" and "/" the same precedence.
    Closes: #359658: units: precedence of '/' is bad

    Units reads ~/.units.dat if it exists before reading the standard
    units database.
    Closes: #456544: units: $UNITSFILE doesn't override default anymore

 -- John Hasler <jhasler@debian.org>  Sun, 20 Jul 2008 21:54:50 -0500

units (1.86a-1) unstable; urgency=low

  * New upstream release

    Upstream has changed this section: we now have usbillion and
    brbillion.  Elsewhere billion is defined as usbillion.  Perhaps
    in the future this could be made to depend on locale.
    Closes: #165946: units: Confusing information in units.dat

    Upstream added a "help" command that searches the database.
    Closes: #375255: units: wishlist (finding the "unit")

    Closes: #370224: crochet is spelled crotchet

  * 'install -s' now only used if 'nostrip' is not in DEB_BUILD_OPTIONS.
    Closes: #438233: units: not handling nostrip build option (policy 10.1)

  * Fixed menu entry.

  * Fixed parallel build problem by consolidating all "install -d"
    commands in debian/rules into one line.

 -- John Hasler <jhasler@debian.org>  Sun, 02 Dec 2007 21:46:50 -0600

units (1.85-3) unstable; urgency=low

  * Fixed typo in control file

  * Added missing build-depends on texinfo
    Closes: #340850: FTBFS: makeinfo: Command not found

 -- John Hasler <jhasler@debian.org>  Wed, 21 Dec 2005 21:10:54 -0600

units (1.85-2) unstable; urgency=low

  * Fixed calling of configure script in rules.
    Closes: #340850: FTBFS: makeinfo: Command not found

  * Upstream changed handling of imperial units (didn't notice earlier).
    Closes: #259411: Mile incorrectly related to other imperial units

 -- John Hasler <jhasler@debian.org>  Tue, 13 Dec 2005 20:56:45 -0600

units (1.85-1) unstable; urgency=low

  * New upstream release
    Closes: #339108 units: new upstream release available
    Closes: #223890 Missing 0 in the help text.
    Closes: #266752 -V: doubled slash
    Closes: #233472 suggest cdromspeed and dvdspeed units
    Closes: #204473 "nmi" should be "nauticalmile", not "nanomile" [patch]

  * Fixed build dependencies.
    Closes: #326284 please rebuild with libreadline5-dev as build dependency
    Closes: #322941 switch to libreadline5

  * Fixed typos.
    Closes: #302822 'man units' typos: "Fahrehneit" and "accomodates"

  * Corrected FSF address.

  * Put missing quotes in menu, moved menu to /usr/share.

 -- John Hasler <jhasler@debian.org>  Tue, 22 Nov 2005 21:33:27 -0600

units (1.81-4) unstable; urgency=low

  * Added md5sums.

 -- John Hasler <jhasler@debian.org>  Tue, 02 Dec 2003 14:30:00 -0500

units (1.81-3) unstable; urgency=low

  * Closes: #195260 FTBFS with gcc-3.3: Uses multiline strings
    Removed example script from end of units.c.  gcc 3.3 was
    choking on it despite it being inside a '#if 0 ... #endif'.

 -- John Hasler <john@dhh.gt.org>  Thu, 29 May 2003 20:30:00 -0500

units (1.81-2) unstable; urgency=low

  * Closes: #168132 FTBFS: missing build-depends
    Added texi2html

 -- John Hasler <john@dhh.gt.org>  Thu, 07 Nov 2002 8:00:00 -0600

units (1.81-1) unstable; urgency=low

  * New upstream version
  * Simplified scripts, added Build-Depends
  * Closes: #150343 New upstream version
  * Closes: #160955 Something Weird About Kelvin-value
    User error
  * Closes: #165955 surveyfoot/intfoot inconsistency
    Fixed in new upstream

 -- John Hasler <john@dhh.gt.org>  Mon, 04 Nov 2002 16:30:00 -0600

units (1.77-1) unstable; urgency=low

  * New upstream version.
  * Closes: #88634
  * Closes: #97022
  * Closes: #88631

 -- John Hasler <john@dhh.gt.org>  Thu, 10 May 2001 20:45:00 -0500

units (1.55-4) unstable; urgency=low

  * Rebuild to use libreadline4.

 -- John Hasler <john@dhh.gt.org>  Mon, 24 Jul 2000 20:45:00 -0500

units (1.55-3) unstable; urgency=low

  * Add readline support.
  * closes: #59589
  * closes: #60900

 -- John Hasler <john@dhh.gt.org>  Wed, 22 Mar 2000 20:45:00 -0500

units (1.55-2) unstable; urgency=low

  * Policy compliance.

 -- John Hasler <john@dhh.gt.org>  Sat, 08 Jan 2000 20:45:00 -0500

units (1.55-1) unstable; urgency=low

  * New upstream release.
  * closes: #43354, #25160

 -- John Hasler <john@dhh.gt.org>  Tue, 03 Aug 1999 20:45:00 -0500

units (1.54-6) unstable; urgency=low

  * Upped libreadlineg2 dependency from -12 to -13 because of
    segfaults with -12.

 -- John Hasler <john@dhh.gt.org>  Sat, 15 May 1999 16:54:37 -0500

units (1.54-5) unstable; urgency=low

  * debian/control (Maintainer): new maintainer.
  * Rebuilt with libc6 2.1.1-1.

 -- John Hasler <john@dhh.gt.org>  Tue, 27 Apr 1999 20:54:37 -0500

units (1.54-4) frozen unstable; urgency=low

  * debian/control (Standards-Version): updated to 2.4.1.0.
  * debian/control (Maintainer): new address.
  * debian/copy: ditto.
  * Rebuilt with ncurses4.
  * units.c: #include <readline/history.h> iff READLINE is defined to get
    add_history() prototype.
  * units.c: Added prototypes for handlesum(), completereduce() and
    compareunits().
  * units.c (readunits,lookupunit,addunit,completeunits): parentheses
    around assignment used as a truth value.
  * units.c (showanswer): initialize sep, right and left to quiet -Wall.
  * debian/rules (binary-arch): install the `NEWS' and `README' files into
    /usr/doc/units/.

 -- James Troup <james@nocrew.org>  Fri, 23 Oct 1998 16:29:34 +0100

units (1.54-3) frozen unstable; urgency=low

  * debian/rules (build): pass datadir=/usr/share/misc/ to configure to
    ensure that units.dat goes to /usr/share/misc/ and doesn't clutter up
    /usr/share/, reported by Scott Ellis <ellis@valueweb.net>. [#20481]
  * debian/urles (binary-arch): ditto (datadir=/debian/tmp/usr/share/misc
    to make install).

 -- James Troup <jjtroup@comp.brad.ac.uk>  Thu, 02 Apr 1998 02:34:53 +0100

units (1.54-2) unstable; urgency=low

  * debian/control (Standards-Version): updated to 2.4.0.0.
  * debian/copyright: correct reference to now uncompressed GPL in
    /usr/doc/copyright.
  * debian/copyright: correct the FSF's address.
  * debian/rules: drop $(package) and use a literal string instead.
  * debian/rules: use go=rX as the argument to chmod, rather than g-ws.
  * debian/rules: don't pass -c to install.
  * debian/rules: other minor cleanups.

 -- James Troup <jjtroup@comp.brad.ac.uk>  Fri, 13 Feb 1998 21:04:47 +0000

units (1.54-1) unstable; urgency=low

  * New upstream version.
  * Uses pristine upstream source.
  * Compile with -g, strip on install.
  * Minor update to postinst. prerm removed and replaced by a more
    intelligent postrm.
  * Minor fixes to copyright file.
  * Added a menu entry.
  * Minor alterations to the rules file (now {}-free *sigh*).

 -- James Troup <jjtroup@comp.brad.ac.uk>  Tue, 18 Nov 1997 03:11:17 +0000

units (1.53-3) unstable; urgency=low

  * Rebuilt with libc6.

 -- James Troup <jjtroup@comp.brad.ac.uk>  Wed, 25 Jun 1997 18:54:32 +0100

units (1.53-2) frozen unstable; urgency=low

  * Patched units.c so that units doesn't go mad when passed null
    arguments.  Patch from Mark Baker <mbaker@iee.org>.
  * Extended units.dat, patch from Mark Baker <mbaker@iee.org> :-
      eushot, thou, ecu, unitedkingdompound, unitedstatesdollar,
      British currency (shilling, oldpence, quid, fiver, tenner),
      English spellings of aluminum and sulfur,
      ISO currency codes (+ UKP).
  * Correct Section: in debian/control (misc -> utils).
  * Replaced cp's with install's in debian/rules.
  * Removed substvars in debian/rules clean target.
  * Added HTML documentation (converted from texinfo) as per standard.

 -- James Troup <jjtroup@comp.brad.ac.uk>  Thu, 03 Apr 1997 00:58:11 +0100

units (1.53-1) unstable; urgency=low

  * Initial release.

 -- James Troup <jjtroup@comp.brad.ac.uk>  Sun, 19 Jan 1997 20:44:33 +0000
